# Keras

## Singularity container with the Keras Deep Learning Library

The Singularity image created is stored on research-singularity-registry.oit.duke.edu. You can pull your image down to the DCC by logging into one of the DCC slogin servers and pulling your image down by curl or wget (e.g. curl -O https://research-singularity-registry.oit.duke.edu/OIT-DCC/keras.img).
